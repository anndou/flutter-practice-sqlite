import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'edit.dart';
import 'provider.dart';
import 'todo_model.dart';

void main() {
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(title: const Text('Todo App')),
          body: const TodoListPage()),
    );
  }
}

class TodoListPage extends ConsumerWidget {
  const TodoListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final AsyncValue<List<Todo>> asyncTodoList = ref.watch(listProvider);
    return Scaffold(
      body: asyncTodoList.when(
        data: (list) => ListView.builder(
            padding: const EdgeInsets.all(8),
            itemCount: list.length,
            itemBuilder: (BuildContext context, int index) {
              return Dismissible(
                  key: ObjectKey(list[index]),
                  onDismissed: (DismissDirection direction) {
                    ref
                        .read(listProvider.notifier)
                        .removeItem(list[index].id ?? 0);
                  },
                  background: Container(
                    color: Colors.red,
                  ),
                  child: todoList(context, list[index], ref));
            }),
        error: ((error, stackTrace) => Text('エラーが発生しました。 ${error.toString()}')),
        loading: () => const Center(child: CircularProgressIndicator()),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Todo todo = Todo(text: await toAddPage(context));
          ref.read(listProvider.notifier).addItem(todo);
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget todoList(BuildContext context, Todo todo, WidgetRef ref) {
    return ListTile(
        title: Text(todo.id.toString() + " : " + todo.text! ?? ""),
        onTap: () async {
          Todo newtodo = Todo(
              id: todo.id, text: await toEditPage(context, todo.text ?? ''));
          ref.read(listProvider.notifier).updateItem(todo.id ?? 0, newtodo);
        });
  }

  Future<String> toEditPage(BuildContext context, String text) async {
    return await Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return TodoEditPage(
        content: text,
      );
    }));
  }

  Future<String> toAddPage(BuildContext context) async {
    return await Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return TodoEditPage(
        content: '',
      );
    }));
  }
}
