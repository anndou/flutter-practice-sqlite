import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'todo_model.dart';
import 'database.dart';

// StateNotifier定義
class ListData extends StateNotifier<AsyncValue<List<Todo>>> {
  TodoDatabase database = TodoDatabase();

  ListData() : super(const AsyncValue.loading()) {
    fetch();
  }

  Future<void> fetch() async {
    final todoList = await database.getTodos();
    state = AsyncValue.data(await database.getTodos());
  }

  void addItem(Todo todo) async {
    database.insert(todo);
    state = AsyncValue.data(await database.getTodos());
  }

  void updateItem(int index, Todo todo) async {
    database.update(todo);
    state = AsyncValue.data(await database.getTodos());
  }

  void removeItem(int index) async {
    database.delete(index);
    state = AsyncValue.data(await database.getTodos());
  }
}
