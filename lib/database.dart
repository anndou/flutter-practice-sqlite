import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'todo_model.dart';

class TodoDatabase {
  late Database database;
  final String tableName = 'todos';

  Future<Database> get getDatabase async {
    database = await initDB();
    return database;
  }

  Future<Database> initDB() async {
    String path = join(await getDatabasesPath(), 'todo.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: createTable,
    );
  }

  Future<void> createTable(Database db, int version) async {
    await db.execute('''
      CREATE TABLE todos(
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        text TEXT
      )
    ''');
  }

  Future<List<Todo>> getTodos() async {
    final db = await getDatabase;
    final maps = await db.query(
      tableName,
      orderBy: 'id ASC',
    );

    if (maps.isEmpty) return [];

    return maps.map((e) => Todo.fromJson(e)).toList();
  }

  Future<Todo> insert(Todo todo) async {
    final db = await getDatabase;
    final id = await db.insert(
      tableName,
      todo.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return Todo(id: id, text: todo.text);
  }

  Future update(Todo todo) async {
    final db = await getDatabase;

    return await db.update(
      tableName,
      todo.toJson(),
      where: 'id = ?',
      whereArgs: [todo.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future delete(int id) async {
    final db = await getDatabase;

    return await db.delete(
      tableName,
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
