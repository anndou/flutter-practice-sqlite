class Todo {
  int? id;
  String? text;
  Todo({this.id, required this.text});

  Todo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['text'] = text;
    return data;
  }
}
