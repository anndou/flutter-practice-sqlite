import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'data.dart';
import 'todo_model.dart';

// Provider定義
final listProvider = StateNotifierProvider<ListData, AsyncValue<List<Todo>>>(
    (ref) => ListData());
